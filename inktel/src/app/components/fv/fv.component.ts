import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { WeatherService } from 'src/app/services/weather.service';
import { Auth } from "@angular/fire/auth";
import Swal from 'sweetalert2';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-fv',
  templateUrl: './fv.component.html',
  styleUrls: ['./fv.component.css']
})
export class FvComponent implements OnInit {
  Uid: any;
  fv :any = [];
  cant = 0;
  showAlert = false;
  infoAlert: any;
  seguro = 0;
  showSpinner = true;
  formAlerts = new FormGroup({
    input1: new FormControl('', Validators.required),
    input2: new FormControl('', Validators.required),
    input3: new FormControl('', Validators.required)
  });
  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });
  type = [
    'Temperature',
    'Wind',
    'Temperature Feels Like',
    'Humidity'
  ]
    
  constructor(private userService: UserService, private api: WeatherService, private auth: Auth) {
  }
  
  ngOnInit(): void {
    this.getUid();
  }
  getUid(){
    this.userService.getUid().then((data:any) =>{
        this.getFv(data);
        this.Uid = data;
    })
  }
  getFv(Uid:any){
    this.userService.getFv(Uid).subscribe(data =>{
      this.getWeather(data, data.length);
    })
  }
  getWeather(info:any, cantidad:number){
    this.fv = [];
    this.seguro = cantidad;
    for (let index = 0; index < this.seguro; index++) {
      this.api.getWeather(info[index].longitud, info[index].latitud).subscribe((data => {
        this.fv.push({data: data, info: info[index]}); 
      }));
    }  
    this.showSpinner = false;    
    this.seguro = 0;  
  }
  onSubmit(info?:any){
    if (this.formAlerts.valid) {
      // console.log(info);
      
      // console.log(this.formAlerts.value);
      // console.log(this.infoAlert.info);
      // this.infoAlert.alerts.
      this.toast('success', 'Alert created successfully')
  
      this.infoAlert = "";
      this.showAlert = false;
    } else {
      this.toast('error', 'Please valide the alert');
    }
  }
  addAlert(info:any){
    this.showAlert = true;
    this.infoAlert = info;
  }
  deleteFv(info:any){
    this.userService.deleteFv(info).then((response) => {
      this.toast('success', "Deleted")
    })
  }
  showAlerts(info:any){
    console.log(info);
  }
  toast(icone:any, tittle:any){
    this.Toast.fire({
      icon: icone,
      title: tittle
    })
  }
}

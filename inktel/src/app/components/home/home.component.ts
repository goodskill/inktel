import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  prueba:any;
  filterForm = new FormGroup({
    country: new FormControl(''),
    zipCode: new FormControl(''),
  });

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });
  
  constructor(private api: WeatherService, private router: Router) { }
  ngOnInit(): void {}
  getZips(zip:any){
    this.api.getZip(zip).subscribe((data =>{
      var zips = data.results
      // console.log(data);
      this.prueba = data.results[`${zip}`];
      if (zips.length == 0) {
        this.toast('error', 'No results, Please search another ZIP');
        this.filterForm.reset();
      }
    }));
  }
  goView(country:any, zip:any, latitud:any, longitud:any){
    // console.log(latitud, longitud);
    this.router.navigate([`weather/${country}/${zip}/${latitud}/${longitud}`]);
  }
  onSubmit(){
    this.getZips(this.filterForm.value.zipCode);
  }

  toast(icone:any, tittle:any){
    this.Toast.fire({
      icon: icone,
      title: tittle
    })
  }


}

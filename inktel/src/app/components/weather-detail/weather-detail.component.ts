import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { WeatherService } from 'src/app/services/weather.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.css']
})
export class WeatherDetailComponent implements OnInit {
  showSpinnerInfo = true;
  showSpinnerFv = true;
  itsFv:any;
  showFv: boolean = true;
  zip:any;
  country: any;
  longitud: any;
  latitud: any;
  weather:any;
  uid:any;
  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });
  constructor(private api: WeatherService, private route: ActivatedRoute, private userService: UserService) {
    this.zip = this.route.snapshot.paramMap.get('zip');
    this.country = this.route.snapshot.paramMap.get('country');
    this.longitud = this.route.snapshot.paramMap.get('longitud');
    this.latitud = this.route.snapshot.paramMap.get('latitud');
    this.getWeather(this.longitud, this.latitud);
  }

  ngOnInit(): void {
    this.getUid();
  }

  getWeather(longitud:string, latitud:string){
    this.api.getWeather(longitud, latitud).subscribe((data => {
      this.weather = data;
      this.getFv();
    }));
  }

  getFv(){
    this.userService.getFv(this.uid).subscribe((data:any)=>{
      const fv = data;
      this.verifyFv(fv);
      this.showSpinnerInfo = false;
    })
  }
  verifyFv(fv:any){
    for (let index = 0; index < fv.length; index++) {
      const found = fv.find((element:any) => element.zip === this.zip);
      if (found) {
        this.showFv = false;
        this.itsFv = found;
      }
    }    
    this.showSpinnerFv = false;
  }

  deleteFv(info:any){
    this.userService.deleteFv(info).then((response) => {
      this.toast('success', response)
    })
  }

  async getUid(){
    this.uid = await this.userService.getUid();
  }

  async addFavorite(){
    if(this.showFv){
      await this.userService.addUser({longitud: this.longitud, latitud: this.latitud, zip: this.zip, uid: this.uid}, this.uid).then((response)=>{
        this.toast('success', 'Great!')
      }).catch(err => {
        this.toast('error', err)
      })
    } else {
      this.userService.deleteFv(this.itsFv).then((response) => {
        this.toast('success', 'Deleted')
      }).catch((error) =>{
        this.toast('error', error)
      })
    } 
    this.showFv = !this.showFv;
  }
  toast(icone:any, tittle:any){
    this.Toast.fire({
      icon: icone,
      title: tittle
    })
  }

}

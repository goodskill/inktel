import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 0,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

  constructor(private userService: UserService, private route: Router) { }

  ngOnInit(): void {
  }

  onLogout(){
    // console.log("debu");
    this.userService.logOut()
      .then(response => {
        this.toast('success', 'LogOut');

        this.route.navigate(['/login']);

      })
      .catch(error => {
        this.toast('error', error)
      })
  }

  toast(icone:any, tittle:any){
    this.Toast.fire({
      icon: icone,
      title: tittle
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

  constructor(private userService: UserService, private route: Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    // console.log(this.registerForm.value);
    this.userService.registerUser(this.registerForm.value)
    .then(response => {
      // console.log(response);
      this.toast('success', "Thanks u for register");
      this.route.navigate(['/login']);
    })
    .catch(error => {
      // console.log(error);
      this.toast('error', error);
    })
  }

  toast(icone:any, tittle:any){
    this.Toast.fire({
      icon: icone,
      title: tittle
    })
  }
}

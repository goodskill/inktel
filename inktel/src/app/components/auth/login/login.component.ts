import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

  constructor(private userService: UserService, private route: Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.userService.loginUser(this.loginForm.value)
    .then(response => {
      this.toast('success', 'Successful Login');
      setTimeout(() => {
        this.route.navigate(['/home']);
      }, 3000);
    })
    .catch(error => {
      this.toast('error', error);
    })
  }

  toast(icone:any, tittle:any){
    this.Toast.fire({
      icon: icone,
      title: tittle
    })
  }

}

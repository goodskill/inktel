import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { MainComponent } from './components/auth/main/main.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { FvComponent } from './components/fv/fv.component';
import { HomeComponent } from './components/home/home.component';
import { WeatherDetailComponent } from './components/weather-detail/weather-detail.component';
import { canActivate, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/home" },
  {
    path: "home", component: HomeComponent,
    ...canActivate(() => redirectUnauthorizedTo(['/login']))
  },
  {
    path: "register", component: RegisterComponent,
    ...canActivate(() => redirectLoggedInTo(['/home']))
  },
  {
    path: "login", component: LoginComponent,
    ...canActivate(() => redirectLoggedInTo(['/home']))
  },
  {
    path: "weather/:country/:zip/:longitud/:latitud", component: WeatherDetailComponent,
    ...canActivate(() => redirectUnauthorizedTo(['/login']))
  },
  {
    path: "favorites", component: FvComponent,
    ...canActivate(() => redirectUnauthorizedTo(['/login']))
  },
  { path: "**", component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

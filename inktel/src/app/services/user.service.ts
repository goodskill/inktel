import { Injectable } from '@angular/core';
import { Auth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, user,  } from "@angular/fire/auth";
import { Firestore, collection, addDoc, collectionData, doc, deleteDoc, updateDoc } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private auth: Auth, private firestore: Firestore) { }

  registerUser({email, password}:any){
    return createUserWithEmailAndPassword(this.auth, email, password);
  }

  loginUser({email, password}:any){
    return signInWithEmailAndPassword(this.auth, email, password);
  }

  logOut(){
    return signOut(this.auth);
  }

  addUser(user:any, type:any){
    const userRef = collection(this.firestore, type);
    return addDoc(userRef, user);
  }

  getFv(type:any):Observable<any>{
    const userRef = collection(this.firestore, type);
    return collectionData(userRef, {idField:'id'}) as Observable<any>;
  }
  
  deleteFv(info:any){
    const infoDocRef = doc(this.firestore, `${info.uid}/${info.id}`);
    return deleteDoc(infoDocRef);
  }

  updateAlerts(info:any){
    // console.log(info);
    const prueba = doc(this.firestore, `${info.uid}/${info.id}`);
    return updateDoc(prueba, {alerts: info.alerts});
  }

  async getUid(){
    const user = await this.auth.currentUser
    var x = (user === undefined ) ?  null : user?.uid;
    return x;
  }

}

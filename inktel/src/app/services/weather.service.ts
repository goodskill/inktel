import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  api = "http://api.weatherunlocked.com/api/current/";
  app_id = "4abb911a";
  app_key = "d3554eb5d4776dfce49014a2324860b9";
  // zip_api = "https://app.zipcodebase.com/api/v1/status?apikey="
  zip_api = "https://app.zipcodebase.com/api/v1/search?apikey="
  zip_key = "33161570-0a01-11ed-9535-cf64907607bc"

  constructor(private http: HttpClient) { }

  public getWeather(latitud:any, longitud:any): Observable<any>{
    return this.http.get<any>(`${this.api}${latitud},${longitud}?app_id=${this.app_id}&app_key=${this.app_key}`);
  }
  public getZip(code1:any, code2?:any):Observable<any>{
    return this.http.get<any>(`${this.zip_api}${this.zip_key}&codes=${code1}`);
   // https://app.zipcodebase.com/api/v1/search?apikey=33161570-0a01-11ed-9535-cf64907607bc&codes=10005%2C51503

  }

}
